# /token

По логину и паролю сотрудника выдает токен, который действует в течение 30 минут.

**URL** : `/token`

**Method** : `POST`

**Auth required** : NO

**Требуемые данные**

```json
{
    "username": "[логин]",
    "password": "[пароль]"
}
```

**Пример данных**

```json
{
    "username": "johndoe",
    "password": "secret"
}
```

## Успешный ответ

**Code** : `200`

**Пример содержимого**

```json
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJqb2huZG9lIiwiZXhwIjoxNjg4Mzc5MTM3fQ.T_ht14-VuKy-Gq2i--DDymR_L3OJWEh_yc9HlquDnZ4",
  "token_type": "bearer"
}
```

## Ошибка

**Условие** : Неправильный пароль или логин.

**Code** : `400`

**Содержимое** :

```json
{
  "detail": "Incorrect username or password"
}
```

_________________________________________________________________

# /pay

Выдает текущую зарплату и дату следующего
повышения по токену.

**URL** : `/pay`

**Method** : `GET`

**Auth required** : YES




## Успешный ответ

**Code** : `200`

**Пример содержимого**

```json
{
  "username": "johndoe",
  "paygrade": 50000,
  "risedate": "08.09.2024"
}
```



