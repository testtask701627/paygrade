import requests
from requests import Response
import logging

from jose import JWTError, jwt
SECRET_KEY = "c94de4ca5d7b3862675021a0d67b3d6b759f51c62b4828f1c359573cf1cf51b1"
ALGORITHM = "HS256"

import pytest


URL = "http://127.0.0.1:8000"
global main_token

class TestToken:
    def test_token(self):      
        body = {"username": "johndoe", "password": "secret"}
        response = requests.post("http://127.0.0.1:8000/token", data={"username": "johndoe", "password": "secret", "grant_type": ""},
                           headers={"content-type": "application/x-www-form-urlencoded"})
        assert response.status_code == 200
        token=response.json()['access_token']
        global main_token
        main_token='Bearer '+token
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("sub")
        assert username == 'johndoe'



class TestPay:
    def test_pay(self):
        global main_token
        response = requests.get("http://127.0.0.1:8000/pay", headers={"Authorization": main_token})
        assert response.status_code == 200
        assert response.json()['username'] == 'johndoe'
        assert response.json()['paygrade'] == 50000
        assert response.json()['risedate'] == "08.09.2024"
